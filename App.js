import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from './src/page/home';
import Add from './src/page/Add';

const Stack = createNativeStackNavigator();

function App(){
  return (
    <NavigationContainer>
      <Stack.Navigator initialRoutName="Home" screenOptions={{headerShown: false}}>
        <Stack.Screen name='Home' component= {Home} />
        <Stack.Screen name='Add' component= {Add} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
export default App;