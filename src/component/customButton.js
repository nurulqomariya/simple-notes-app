import React from 'react';
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';

const customButton = props => {
  return (
    <View>
      <TouchableOpacity onPress={props.onPress} style={styles.btn}>
        <Text style={styles.text}>{props.label}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default customButton;

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#2c3e50',
    padding: 10,
    borderRadius: 50,
    margin: 10,
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
