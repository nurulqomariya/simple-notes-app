import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
} from 'react-native';
import CustomButton from '../component/customButton';

let notes = [
  {judul: 'Senin', subTitle: 'ini data 1'},
  {judul: 'Selasa', subTitle: 'ini data 2'},
  {judul: 'Rabu', subTitle: 'ini data 3'},
  {judul: 'Kamis', subTitle: 'ini data 4'},
  {judul: 'Jumat', subTitle: 'ini data 5'},
  {judul: 'Sabtu', subTitle: 'ini data 6'},
  {judul: 'Minggu', subTitle: 'ini data 7'},
  {judul: 'Sekarang', subTitle: 'ini data 8'},
];
export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cari: '',
      title: notes,
    };
  }

  search = () => {
    let data = notes;
    data = data.filter(item =>
      item.judul
        .toLocaleLowerCase()
        .includes(this.state.cari.toLocaleLowerCase()),
    );
    this.setState({
      title: data,
    });
  };
  render() {
    return (
      <View>
        <StatusBar backgroundColor="#2c3e50" barStyle="light-content" />
        <View style={style.kotak}>
          <Text style={style.tagName}>Notes</Text>
        </View>
        <TextInput
          onChangeText={text => this.setState({cari: text})}
          value={this.state.cari}
          placeholder="Ketikkan Sesuatu"
          onKeyPress={() => this.search()}
          style={style.input}
        />
        <FlatList
          data={this.state.title}
          renderItem={({item}) => (
            <View style={style.flat}>
              <Text>{item.judul}</Text>
              <Text>{item.subTitle}</Text>
            </View>
          )}
          keyExtractor={item => item.judul}
        />
        <CustomButton label="Tambah Catatan" onPress={() => this.props.navigation.navigate('Add')} />
      </View>
    );
  }
}

export default Home;

const style = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderRadius: 20,
    marginHorizontal: 10,
    marginVertical: 10,
    padding: 10,
  },
  flat: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 5,
  },
  kotak: {
    padding: 20,
    backgroundColor: '#2c3e50',
    shadowColor: '#000',
    shadowOpacity: 0.32,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5.46,
    elevation: 9,
  },
  tagName: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
  },
});
