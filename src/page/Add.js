import React, { Component } from 'react'
import { View, TextInput, Text } from 'react-native'
import CustomButton from '../component/customButton'

export class Add extends Component{
    state = {
        title : ''
    }
    render(){
        return (
            <View>
                <TextInput placeholder="Tambah Catatan" onChangeText={text => this.setState({title: text})} />
                <Text>{this.state.title}</Text>
                <CustomButton label="Save" />
            </View>
        )
    }
}

export default Add
